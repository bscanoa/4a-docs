## Miembros del equipo 

| Nombre 								| Mail  | Rol  |
| ------------ | ------------ | ------------ |
| Brian Stiven Alvarado Cano |  bscanoa@gmail.com | Scrum Master |
| Adriana Perez  | adrianaperezap058@gmail.com  | Product Owner  |
| Ana Gabriela Teran Almeida  | ateran@unal.edu.co  | Developer  |
| Juan Diego Lozada González  | juandiegolozada123@gmail.com  | Developer  |
| Juan José Portilla Rodriguez  | u20162153114@usco.edu.co  | Developer  |