# Documentación Sprint 1

##### - Crear un sitio en Jira

El sitio que alojará el proyecto será uno utilizado en el ciclo anterio, el cual será administrado por el Scrum Master. 

[![Sitio](https://i.imgur.com/k2WpTjg.png "Sitio")](https://i.imgur.com/k2WpTjg.png "Sitio")

##### - Crear un proyecto en Jira

[![Proyecto](https://i.imgur.com/0OTNfVc.png "Proyecto")](https://i.imgur.com/0OTNfVc.png "Proyecto")

##### - Añadir a los miembros del equipo y al respectivo instructor.

[![Miembros](https://i.imgur.com/GUN129u.png "Miembros")](https://i.imgur.com/GUN129u.png "Miembros")

##### - Crear el Sprint 1

Las tres incidencias correspondientes al sprint se pueden visualizar a continuación.

[![Sprint 1](https://i.imgur.com/iLCf3ks.png "Sprint 1")](https://i.imgur.com/iLCf3ks.png "Sprint 1")

##### - Crear un repositorio llamado 4a-docs

[![repo](https://i.imgur.com/a03zAcU.png "repo")](https://i.imgur.com/a03zAcU.png "repo")

##### - Asociar el repositorio creado a Jira

[![RepoInJira](https://i.imgur.com/xac1wzL.png "RepoInJira")](https://i.imgur.com/xac1wzL.png "RepoInJira")

##### - Definir problema a solucionar mediante el Aplicativo Web

El aplicativo web que se construirá tiene como objetivo permitir al usuario acceder de forma fácil y efectiva a movibilidad temporal en una ciudad dada. Este servicio responde a la necesidad del cliente al momento de llegar a una nueva ciudad y no encontrar servicios efectivos en la renta de carros.

- Permitir al usuario realizar reservaciones desde el aplicativo. 

- Brindar una opción de servicios “domicilario”. Donde el cliente solicite el auto a un lugar especifico. 

- Generar facturas para facilitar el pago del servicio.

- Construir un sitio rápido e intuitivo. Permitiendo al cliente poder acceder facilmente al servicio.

- Crear CRUD para manipular el invetario de carros. 

##### - Crear una lista de historias de usuario

- Mostrar los carros disponibles con detalles y precios.
- Filtrar por marcas de carro.
- Filtrar los carros por categorías (carro / camietas). 
- Permitir al usuario crear una cuenta dentro del aplicativo.
- Permitir al usuario loguearse con con correo y contraseña. 
- Dar acceso a un formulario para reservar el carro.  
- Dar al usuario la posibilidad de cancelar una reserva. 
- Cuando se reserve un carro que no aparezca o que muestre su estado de RESERVADO.
- Generar una factura. Y sobre la factura especificar sobrecostos de tanqueo o lavado.
- Formulario de entrega, que incluya el estado del vehículo y condiciones. Si no cumple alguna condición aumentará el costo en la factura final
- Permitir al usuario revisar su historial de reservas.
- Poder visualizar las facturas.
- Como usuario administrador me permita crear, borrar o actualizar los carros disponibles.
- Visualizar las reservaciones.


##### - Diseñar una versión preliminar de la aplicación 

[![Arq-Pre](https://i.imgur.com/SCzCvGE.jpg "Arq-Pre")](https://i.imgur.com/SCzCvGE.jpg "Arq-Pre")
